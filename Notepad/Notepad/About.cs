﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Notepad
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
        }

        private void About_Load(object sender, EventArgs e)
        {
            Image ima = Image.FromFile("c:\\Notepad-Bloc-notes-icon.png");
            Label l1 = new Label();
            l1.Location = new Point(52, 13);
            l1.Size = new Size(ima.Width, ima.Height);
            l1.Image = ima;
            lpn.Text = string.Format("Product Name is: {0}", Application.ProductName);
            lpv.Text = string.Format("Version: {0}", Application.ProductVersion);
            lc.Text = "Copyright ©  2020 by ManueLord";
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
