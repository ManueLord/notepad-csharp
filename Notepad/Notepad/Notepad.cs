﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Notepad
{
    public partial class Notepad : Form
    {
        private static string FilePath = "";
        public static int count = 0;
        public Notepad()
        {
            InitializeComponent();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            count++;
            Notepad np = new Notepad();
            np.Text = "New Document " + count.ToString();
            np.Show();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Open...";
            ofd.Filter = "Text Document (*.txt)|*.txt|All Files(*.*)|*.*";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                richTextBox1.LoadFile(ofd.FileName, RichTextBoxStreamType.PlainText);
            }
            this.Text = ofd.FileName;
            FilePath = ofd.FileName;
        }

        private void btnSave_Click(object sender, EventArgs e)
        { 
            if (!FilePath.Equals(""))
            {
                DialogResult dr = MessageBox.Show("Are you sure to save ?", "Save", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    File.WriteAllText(FilePath, richTextBox1.Text);
                }
            }
            else
            {
                btnSaveAs_Click(sender, e);
            }
        }

        private void btnSaveAs_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Save As...";
            sfd.Filter = "Text Document (*.txt)|*.txt|All Files(*.*)|*.*";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                richTextBox1.SaveFile(sfd.FileName, RichTextBoxStreamType.PlainText);
            }
            FilePath = sfd.FileName;
            this.Text = sfd.FileName;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCut_Click(object sender, EventArgs e)
        {
            richTextBox1.Cut();
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            richTextBox1.Copy();
        }

        private void btnPaste_Click(object sender, EventArgs e)
        {
            richTextBox1.Paste();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = null;
        }

        private void btnFontSize_Click(object sender, EventArgs e)
        {
            FontDialog fd = new FontDialog();
            if(fd.ShowDialog() == DialogResult.OK)
            {
                richTextBox1.Font = fd.Font;
            }
        }

        private void btnFontColor_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();
            cd.ShowDialog();
            richTextBox1.SelectionColor = cd.Color;
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            About a = new About();
            a.Show();
        }
    }
}
